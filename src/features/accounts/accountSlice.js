import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	balance: 0,
	loan: 0,
	loanPurpose: ''
};

const accountSlice = createSlice({
	name: "account",
	initialState,
	reducers: {
		deposit(state, action) {
			state.balance = state.balance + action.payload;
		},
		withdraw(state, action) {
			state.balance = state.balance - action.payload;
		},
		requestLoan: {
			// this done : due purpose varibale missig , only amount came . so if such problem we can prepare it before pass.
			prepare ( amount, purpose )
			{
				return {
					payload: { amount, purpose }
				};
			},
			reducer ( state, action )
			{
				if ( state.loan > 0 ) return;
				state.loan = action.payload.amount;
				state.purpose = action.payload.purpose;
				state.balance = state.balance + action.payload.amount;
			}
		},
		payLoan(state) {
			state.balance -= state.loan;
			state.loan = 0;
			state.loanPurpose = '';
		}
	}

});

console.log(accountSlice.initialState);

export const { withdraw,requestLoan,payLoan} = accountSlice.actions
export default accountSlice.reducer;


export function deposit ( amount,currency )
{
	if (currency==='USD') {
			return { type :'account/deposit', payload: amount}
	}
	return async function (dispatch, getState)
	{

		const res =await fetch(`https://api.frankfurter.app/latest?amount=${amount}&from=${currency}&to=USD`)
		const data = await res.json()
		const converted = data.rates.USD;
		console.log('conv ', converted);

		dispatch ({ type: 'account/deposit', payload: converted })

	}

}


// export default function accountReducer(state = intialState, action) {
// 	switch (action.type) {
// 		case 'account/deposit':
// 			return { ...state, balance: state.balance + action.payload };
// 		case 'account/withdraw':
// 			return { ...state, balance: state.balance - action.payload };
// 		case 'account/requestLoan':
// 			return {
// 				...state,
// 				loan: action.payload.amount,
// 				balance: state.balance + action.payload.amount,
// 				loanPurpose: action.payload.purpose
// 			};
// 		case 'account/payLoan':
// 			if (state.loan > 0) return state;
// 			return {
// 				...state,
// 				balance: state.balance - state.loan,
// 				loan: 0,
// 				loanPurpose: ''
// 			};

//     default:
//       return state;
// 	}
// }

// export function deposite ( amount,currency )
// {
// 	if (currency==='USD') {
// 			return { type :'account/deposit', payload: amount}
// 	}
// 	return async function (dispatch, getState)
// 	{

// 		const res =await fetch(`https://api.frankfurter.app/latest?amount=${amount}&from=${currency}&to=USD`)
// 		const data = await res.json()
// 		const converted = data.rates.USD;
// 		console.log('conv ', converted);

// 		dispatch ({ type: 'account/deposit', payload: converted })

// 	}

// }
// export function withdraw ( amount )
// {
// 	return { type :'account/withdraw', payload: amount}

// }
// export function requestLoan ( amount, purpose )
// {
// 	return { type :'account/requestLoan', payload: {amount, purpose}}

// }

// export function payLoan ( )
// {
// 	return { type: 'account/payLoan' }
// }
