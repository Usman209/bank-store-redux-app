const intialState = {
	balance: 0,
	loan: 0,
	loanPurpose: ''
};
export default function accountReducer(state = intialState, action) {
	switch (action.type) {
		case 'account/deposit':
			return { ...state, balance: state.balance + action.payload };
		case 'account/withdraw':
			return { ...state, balance: state.balance - action.payload };
		case 'account/requestLoan':
			return {
				...state,
				loan: action.payload.amount,
				balance: state.balance + action.payload.amount,
				loanPurpose: action.payload.purpose
			};
		case 'account/payLoan':
			if (state.loan > 0) return state;
			return {
				...state,
				balance: state.balance - state.loan,
				loan: 0,
				loanPurpose: ''
			};

    default:
      return state;
	}
}

export function deposite ( amount,currency )
{
	if (currency==='USD') {
			return { type :'account/deposit', payload: amount}
	}
	return async function (dispatch, getState)
	{

		const res =await fetch(`https://api.frankfurter.app/latest?amount=${amount}&from=${currency}&to=USD`)
		const data = await res.json()
		const converted = data.rates.USD;
		console.log('conv ', converted);
					
		dispatch ({ type: 'account/deposit', payload: converted })


	}
	
}
export function withdraw ( amount )
{
	return { type :'account/withdraw', payload: amount}
	
}
export function requestLoan ( amount, purpose )
{
	return { type :'account/requestLoan', payload: {amount, purpose}}
	
}

export function payLoan ( )
{
	return { type: 'account/payLoan' }
}
	


